# Chrome To Nozbe

Create a Nozbe task right from your browser!

<img src="screenshot.png" align="right" title="Screenshot" width="25%">

## What is it?
*Chrome to Nozbe* is a Chrome extension allowing you to save the current tab URL to the Nozbe productivity system. It allows you to add a proper task name and select the project you want to save the link to.

## Contributing
If you have comments, complaints, or ideas for improvements, feel free to open an issue or a pull request! 

The extension uses an npm package written by me ([node-nozbe](https://github.com/kkoscielniak/node-nozbe)) to talk with the Nozbe servers. I've used this [boilerplate](https://github.com/samuelsimoes/chrome-extension-webpack-boilerplate) to kickstart the extension development.

## License

Chrome To Nozbe was created by [Krystian Kościelniak](https://koscielniak.pro). It is available under the MIT license. See the LICENSE file for more info.
