export const getAccessToken = async () => {
  try {
    const result = await chrome.storage.local.get('accessToken');

    if (result.accessToken) {
      return result.accessToken.accessToken;
    }
  } catch (e) {
    console.error(e);
  }
};

export const setAccessToken = async accessToken => {
  try {
    await chrome.storage.local.set({ accessToken });
  } catch (e) {
    console.error(e);
  }
};

export const clearStorage = async () => {
  try {
    await chrome.storage.local.clear();
  } catch (e) {
    console.error(e);
  }
};
