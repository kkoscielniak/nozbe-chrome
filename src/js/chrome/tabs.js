export const getCurrentTab = async () => {
  try {
    const tabs = await chrome.tabs.query({
      active: true,
      currentWindow: true,
    });
    const activeTab = tabs[0];

    return activeTab;
  } catch (e) {
    console.error(e);
  }
};
