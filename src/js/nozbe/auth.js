import Nozbe from 'node-nozbe';

import { NOZBE } from '../secrets.dev';
import { setAccessToken } from '../chrome/storage';

const getAccessTokenFromUrl = urlString => {
  const url = new URL(urlString);
  return url.searchParams.get('access_token');
};

export const authenticate = async () => {
  try {
    const redirectPath = Nozbe.getOAuthLoginURL(NOZBE.client_id);

    await chrome.identity.launchWebAuthFlow(
      {
        url: redirectPath,
        interactive: true,
      },
      async callbackUrl => {
        const accessToken = getAccessTokenFromUrl(callbackUrl);

        await setAccessToken({ accessToken });
      },
    );
  } catch (e) {
    console.error(e);
  }
};
