export const events = {
  ADD_TASK: 'add-task',
};

export const addTaskStatuses = {
  IDLE: 'idle',
  ADDING: 'adding',
  ADDED: 'added',
};
