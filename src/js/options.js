import { clearStorage } from './chrome/storage';

const clearTheStorage = async () => {
  await clearStorage();
  alert('You are logged out.');
};

document
  .getElementById('clearStorage')
  .addEventListener('click', clearTheStorage);
