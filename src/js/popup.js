import 'vue-material/dist/vue-material.min.css';
import '../css/popup.scss';

import 'chrome-extension-async';
import Vue from 'vue/dist/vue.common';
import VueMaterial from 'vue-material';

import { addTaskStatuses } from './constants';
import { getAccessToken } from './chrome/storage';
import AddTaskForm from './components/addTaskForm.component.vue';
import AuthenticationForm from './components/authenticationForm.component.vue';

Vue.use(VueMaterial);

const vm = new Vue({
  el: '#mainWindow',
  data: {
    accessToken: null,
    status: addTaskStatuses.IDLE,
  },
  components: {
    AddTaskForm,
    AuthenticationForm,
  },
  mounted: async () => {
    let accessToken = await getAccessToken();
    vm.accessToken = accessToken;
  },
  methods: {
    authenticated: accessToken => {
      vm.accessToken = accessToken;
    },
    openSettings: () => {
      chrome.runtime.openOptionsPage();
    },
    statusChanged: status => {
      vm.status = status;
    },
  },
});
